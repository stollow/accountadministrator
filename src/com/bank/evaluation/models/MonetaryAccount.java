package com.bank.evaluation.models;

public class MonetaryAccount {
    private double amount;
    private String currency;

    public MonetaryAccount(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public void addAmount(double a){
        this.amount+= a;

    }

    public void substractAmount(double a){
        this.amount -= a;
    }

    public double getAmount(){
        return this.amount;
    }

    @Override
    public String toString() {
        return "MonetaryAccount{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }
}
