package com.bank.evaluation.models;

public class Account {
    private MonetaryAccount balance;
    private String ownerName;

    public Account(String ownerName, String currency){
        this.ownerName = ownerName;
        this.balance = new MonetaryAccount(0.0,currency);
    }

    public void deposit(double amount){
        this.balance.addAmount(amount);
    }

    public void withdraw(double amount){
        if(balance.getAmount()-amount >0) {
            this.balance.substractAmount(amount);
        }
    }

    public MonetaryAccount getCurrentBalance(){
        return this.balance;
    }

    public String getOwnerName() {
        return ownerName;
    }
}
