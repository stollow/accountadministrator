package com.bank.evaluation;

import com.bank.evaluation.models.Account;

public class AccountAdministrator {
    public static void main(String[] args) {
        Account account = new Account("Obélix","sesterces");
        System.out.println(account.getCurrentBalance().toString());
        account.deposit(500.5);
        System.out.println(account.getCurrentBalance().toString());
        account.withdraw(125.25);
        System.out.println(account.getCurrentBalance().toString());
        account.withdraw(1337.0);
        System.out.println(account.getCurrentBalance().toString());
    }
}
